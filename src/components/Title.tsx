import React from "react";

export default class Title extends React.Component {

    private static siteTitle: string = 'Scans.loc';

    public static set(newTitle: string): void {
        document.title = `${newTitle} - ${this.siteTitle}`;
    }
}
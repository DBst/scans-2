import React, {useEffect, useState} from "react";
import {Translations as T} from "../../components/Translations";
import {
    MDBContainer,
    MDBNavbar,
    MDBNavbarToggler,
    MDBIcon,
    MDBNavbarNav,
    MDBNavbarItem,
    MDBNavbarLink,
    MDBBtn,
    MDBDropdown,
    MDBDropdownToggle,
    MDBDropdownMenu,
    MDBDropdownItem,
    MDBDropdownLink,
    MDBCollapse
} from 'mdb-react-ui-kit';
import './layouts.scss';
import {Link} from 'react-router-dom';

const Navbar = () => {

    const [showBasic, setShowBasic] = useState(false);

    const [fixedTop, setFixedTop] = useState(false);
    const [navPos, setNavPos] = useState(0);

    const onScroll = () => {

        const currentPos = window.scrollY;

        if(currentPos < 100) {
            setFixedTop(false);
        } else if(!fixedTop) {
            setNavPos(-60);

            setTimeout(() => {
                setFixedTop(true);
                setNavPos(0);
            }, 150);
        }

    }

    const getUrlForLanguage = (language: string): string => {

        if(!Object.keys(T.languages).includes(language)) {
            new Error('Cannot find that language!');
        }

        let domain = window.location.host,
            s = domain.split('.'),
            domainWithoutLang = s[0].length === 2 && Object.keys(T.languages).includes(s[0]) ? s.splice(1).join('.') : domain;

        return `${window.location.protocol}//${ language === 'en' ? '' : `${language}.` }${ domainWithoutLang }${window.location.pathname}`;
    }

    useEffect(() => {

        window.addEventListener('scroll', onScroll);
        return () => window.removeEventListener('scroll', onScroll);

    }, [fixedTop, navPos, onScroll]);

    return (
        <div id={'nav-section'}>
            <div style={{height: window.innerWidth > 992 ? 41.6 : 0}}>
                <MDBNavbar expand='lg' light bgColor='light' style={{top: navPos}} className={`p-0 d-none d-lg-block ${fixedTop ? "fixed-navbar" : ""}`}>
                    <MDBContainer>

                        <MDBDropdown animation={false}>
                            <MDBDropdownToggle tag='a' className='nav-link ps-0 pe-2' style={{
                                color: 'rgba(0,0,0,.55)'
                            }}>
                                <MDBIcon icon='bars' fas />
                            </MDBDropdownToggle>
                            <MDBDropdownMenu>
                                <MDBDropdownItem>
                                    <Link to={'/'} className={'dropdown-item'}>
                                        <MDBIcon icon='home' fas /> <T>Home</T>
                                    </Link>
                                </MDBDropdownItem>
                                <MDBDropdownItem>
                                    <Link to={'/manga/list/1'} className={'dropdown-item'}>
                                        <MDBIcon icon='stream' fas /> <T>Manga list</T>
                                    </Link>
                                </MDBDropdownItem>
                                <MDBDropdown dropright={true} animation={false}>
                                    <MDBDropdownToggle tag='a' className='nav-link' style={{color: 'black'}}>
                                        <MDBIcon icon='bars' fas /> <T>For creators</T>
                                    </MDBDropdownToggle>
                                    <MDBDropdownMenu>
                                        <MDBDropdownItem>
                                            <Link to={'/chapter/upload'} className={'dropdown-item'}>
                                                <MDBIcon icon='cloud-upload-alt' fas /> <T>Upload chapter</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <Link to={'/chapter/my-chapters'} className={'dropdown-item'}>
                                                <MDBIcon icon='stream' fas /> <T>My chapters</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <Link to={'/group'} className={'dropdown-item'}>
                                                <MDBIcon icon='users' fas /> <T>Group management</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <Link to={'/manga/suggest'} className={'dropdown-item'}>
                                                <MDBIcon icon='book' fas /> <T>Suggest a manga</T>
                                            </Link>
                                        </MDBDropdownItem>
                                    </MDBDropdownMenu>
                                </MDBDropdown>
                            </MDBDropdownMenu>
                        </MDBDropdown>

                        <MDBDropdown animation={false}>
                            <MDBDropdownToggle tag='a' className='nav-link ps-1 pe-2' style={{color: 'rgba(0,0,0,.55)'}}>
                                <MDBIcon icon='globe' fas />
                            </MDBDropdownToggle>
                            <MDBDropdownMenu>
                                <div className={'row'} style={{width: '350px'}}>
                                    <div className={'col-6'}>
                                        <span className="mt-1 mb-1 d-block fw-bold site-language">
                                            <T>Site Language</T>
                                        </span>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink href={getUrlForLanguage('en')}>
                                                <MDBIcon flag='uk' /> <T>English</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink href={getUrlForLanguage('pl')}>
                                                <MDBIcon flag='pl' /> <T>Polish</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink href={getUrlForLanguage('de')}>
                                                <MDBIcon flag='de' /> <T>German</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink href={getUrlForLanguage('ua')}>
                                                <MDBIcon flag='ua' /> <T>Ukrainian</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink href={getUrlForLanguage('ru')}>
                                                <MDBIcon flag='ru' /> <T>Russian</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                    </div>
                                    <div className={'col-6'}>
                                        <span className="mt-1 mb-1 d-block fw-bold chapter-language">
                                            <T>Chapter Language</T>
                                        </span>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>
                                                <MDBIcon flag='uk' /> <T>English</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>
                                                <MDBIcon flag='pl' /> <T>Polish</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>
                                                <MDBIcon flag='de' /> <T>German</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>
                                                <MDBIcon flag='ua' /> <T>Ukrainian</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>
                                                <MDBIcon flag='ru' /> <T>Russian</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                    </div>
                                </div>
                            </MDBDropdownMenu>
                        </MDBDropdown>

                        <form className='d-flex input-group w-auto'>
                            <input type='search' className='form-control' placeholder='Search' aria-label='Search' />
                            <MDBBtn className={'navbar-search-btn'} color='white' style={{border: '1px solid #bdbdbd', borderLeft: 'unset'}}>
                                <MDBIcon icon='search' fas />
                            </MDBBtn>
                        </form>

                        <MDBNavbarNav className='ml-auto mb-2 mb-lg-0 justify-content-end'>

                            <MDBNavbarItem>
                                <MDBNavbarLink active aria-current='page' href='#' style={{color: 'rgba(0,0,0,.55)'}}>
                                    <MDBIcon icon='bell' fas />
                                </MDBNavbarLink>
                            </MDBNavbarItem>

                            <MDBNavbarItem>
                                <MDBDropdown animation={false}>
                                    <MDBDropdownToggle tag='a' className='nav-link pe-0' style={{color: 'rgba(0,0,0,.55)'}}>
                                        <MDBIcon icon='user' fas />
                                    </MDBDropdownToggle>
                                    <MDBDropdownMenu>
                                        <MDBDropdownItem>
                                            <Link to={'/manga/watchlist'} className={'dropdown-item'}>
                                                <MDBIcon icon='bell' fas /> <T>Watchlist</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <Link to={'/user/settings'} className={'dropdown-item'}>
                                                <MDBIcon icon='cog' fas /> <T>Settings</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <div className="dropdown-divider"></div>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>
                                                <MDBIcon icon='sign-out-alt' fas /> <T>Logout</T>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                    </MDBDropdownMenu>
                                </MDBDropdown>
                            </MDBNavbarItem>

                        </MDBNavbarNav>

                    </MDBContainer>
                </MDBNavbar>
            </div>
            <MDBNavbar expand='lg' dark bgColor='primary'>
                <MDBContainer>
                    <Link to={'/'} className={'navbar-brand'}>
                        Scans 2.0
                    </Link>

                    <MDBNavbarToggler
                        aria-controls='navbarSupportedContent'
                        aria-expanded='false'
                        aria-label='Toggle navigation'
                        onClick={() => setShowBasic(!showBasic)}
                    >
                        <MDBIcon icon='bars-staggered' fas />
                    </MDBNavbarToggler>

                    <MDBCollapse navbar show={showBasic}>
                        <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>

                            <form className='d-flex input-group w-auto mt-3 mb-3 d-lg-none d-block'>
                                <input type='search' className='form-control' placeholder='Search' aria-label='Search' />
                                <MDBBtn className={'navbar-search-btn'} color='white' style={{border: '1px solid #bdbdbd', borderLeft: 'unset'}}>
                                    <MDBIcon icon='search' fas />
                                </MDBBtn>
                            </form>

                            <MDBNavbarItem>
                                <Link to={'/'} className={'nav-link'}>
                                    <MDBIcon icon='home' fas /> <T>Home</T>
                                </Link>
                            </MDBNavbarItem>
                            <MDBNavbarItem>
                                <Link to={'/manga/list/1'} className={'nav-link'}>
                                    <MDBIcon icon='stream' fas /> <T>Manga list</T>
                                </Link>
                            </MDBNavbarItem>

                            <MDBNavbarItem>
                                <MDBDropdown animation={false}>
                                    <MDBDropdownToggle tag='a' className='nav-link'>
                                        <MDBIcon icon='folder' fas /> <T>For creators</T>
                                    </MDBDropdownToggle>
                                    <MDBDropdownMenu>

                                        <MDBDropdownItem>
                                            <Link to={'/chapter/upload'} className={'dropdown-item'}>
                                                <MDBIcon icon='cloud-upload-alt' fas /> <T>Upload chapter</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <Link to={'/chapter/my-chapters'} className={'dropdown-item'}>
                                                <MDBIcon icon='stream' fas /> <T>My chapters</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <Link to={'/group'} className={'dropdown-item'}>
                                                <MDBIcon icon='users' fas /> <T>Group management</T>
                                            </Link>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <Link to={'/manga/suggest'} className={'dropdown-item'}>
                                                <MDBIcon icon='book' fas /> <T>Suggest a manga</T>
                                            </Link>
                                        </MDBDropdownItem>
                                    </MDBDropdownMenu>
                                </MDBDropdown>
                            </MDBNavbarItem>

                            <div className={'d-flex justify-content-between ms-5 me-5 d-lg-none d-block'}>

                                <MDBNavbarItem>
                                    <MDBNavbarLink aria-current='page' style={{color: 'rgba(0,0,0,.55)'}}>
                                        <MDBIcon className={'text-white'} icon='bell' fas />
                                    </MDBNavbarLink>
                                </MDBNavbarItem>

                                <MDBNavbarItem>
                                    <MDBDropdown animation={false}>
                                        <MDBDropdownToggle tag='a' className='nav-link pe-0' style={{color: 'rgba(0,0,0,.55)'}}>
                                            <MDBIcon className={'text-white'} icon='user' fas />
                                        </MDBDropdownToggle>
                                        <MDBDropdownMenu>
                                            <MDBDropdownItem>
                                                <Link to={'/manga/watchlist'} className={'dropdown-item'}>
                                                    <MDBIcon icon='bell' fas /> <T>Watchlist</T>
                                                </Link>
                                            </MDBDropdownItem>
                                            <MDBDropdownItem>
                                                <Link to={'/user/settings'} className={'dropdown-item'}>
                                                    <MDBIcon icon='cog' fas /> <T>Settings</T>
                                                </Link>
                                            </MDBDropdownItem>
                                            <div className="dropdown-divider"></div>
                                            <MDBDropdownItem>
                                                <MDBDropdownLink>
                                                    <MDBIcon icon='sign-out-alt' fas /> <T>Logout</T>
                                                </MDBDropdownLink>
                                            </MDBDropdownItem>
                                        </MDBDropdownMenu>
                                    </MDBDropdown>
                                </MDBNavbarItem>

                                <MDBDropdown animation={false}>
                                    <MDBDropdownToggle tag='a' className='nav-link ps-1 pe-2' style={{color: 'rgba(0,0,0,.55)'}}>
                                        <MDBIcon className={'text-white'} icon='globe' fas />
                                    </MDBDropdownToggle>
                                    <MDBDropdownMenu>
                                        <div className={'row'} style={{width: '350px'}}>
                                            <div className={'col-6'}>
                                                <span className="mt-1 mb-1 d-block fw-bold site-language">
                                                    <T>Site Language</T>
                                                </span>
                                                <MDBDropdownItem>
                                                    <MDBDropdownLink>
                                                        <MDBIcon flag='uk' /> <T>English</T>
                                                    </MDBDropdownLink>
                                                </MDBDropdownItem>
                                                <MDBDropdownItem>
                                                    <MDBDropdownLink>
                                                        <MDBIcon flag='pl' /> <T>Polish</T>
                                                    </MDBDropdownLink>
                                                </MDBDropdownItem>
                                            </div>
                                            <div className={'col-6'}>
                                                <span className="mt-1 mb-1 d-block fw-bold chapter-language">
                                                    <T>Chapter Language</T>
                                                </span>
                                                <MDBDropdownItem>
                                                    <MDBDropdownLink>
                                                        <MDBIcon flag='uk' /> <T>English</T>
                                                    </MDBDropdownLink>
                                                </MDBDropdownItem>
                                                <MDBDropdownItem>
                                                    <MDBDropdownLink>
                                                        <MDBIcon flag='pl' /> <T>Polish</T>
                                                    </MDBDropdownLink>
                                                </MDBDropdownItem>
                                                <MDBDropdownItem>
                                                    <MDBDropdownLink>
                                                        <MDBIcon flag='de' /> <T>German</T>
                                                    </MDBDropdownLink>
                                                </MDBDropdownItem>
                                                <MDBDropdownItem>
                                                    <MDBDropdownLink>
                                                        <MDBIcon flag='ua' /> <T>Ukrainian</T>
                                                    </MDBDropdownLink>
                                                </MDBDropdownItem>
                                                <MDBDropdownItem>
                                                    <MDBDropdownLink>
                                                        <MDBIcon flag='ru' /> <T>Russian</T>
                                                    </MDBDropdownLink>
                                                </MDBDropdownItem>
                                            </div>
                                        </div>
                                    </MDBDropdownMenu>
                                </MDBDropdown>

                            </div>

                        </MDBNavbarNav>

                    </MDBCollapse>
                </MDBContainer>
            </MDBNavbar>
        </div>
    );
}
export default Navbar;
import React from "react";
import './layouts.scss';

export default class Loader extends React.Component<{}, {
    width: number,
    interval: any,
    display: string,
}> {

    public static run(): void {
        let width = 0;
        let c = 0;

        setTimeout(() => {
            window.Loader.setState({display: 'block'});

            let interval = setInterval(() => {

                c++;
                width = c / (c + 5) * 100;

                window.Loader.setState({width: width});

            }, 50);
            window.Loader.setState({
                interval: interval
            });
        });
    }

    public static done(): void {

        setTimeout(() => {

            clearInterval(window.Loader.state.interval);
            window.Loader.setState({width: 100});

            setTimeout(() => {
                window.Loader.setState({width: 0});
                window.Loader.setState({display: 'none'});
            }, 250);

        });

    }

    constructor(props: any) {
        super(props);
        this.state = {
            width: 0,
            interval: 0,
            display: 'none',
        };
    }

    render() {
        return <div id={'loader'} className={'bg-primary'} style={{width: `${this.state.width}%`, display: this.state.display}}></div>;
    }

}

export default class Russian {

    private static translations = {
        'Home': 'Дом',
    };

    public static get(key: string) {
        return this.translations[key as keyof typeof Russian.translations];
    }

}
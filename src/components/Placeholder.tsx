
const Placeholder = (props: any) => {

    let width = props.width ? `${props.width}` : '50%';
    if(!width.endsWith('%') && !width.endsWith('px')) {
        width += 'px';
    }

    return (
        <div className={'placeholder-glow'}>
            <div className={'placeholder'} style={{width: width}}>

            </div>
        </div>
    );
}
export default Placeholder;
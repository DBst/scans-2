import React from "react";
import {
    Routes,
    Route
} from "react-router-dom";
import Index from "./site/Index";
import List from "./manga/List";
import Loader from "./layouts/Loader";
import Navbar from "./layouts/Navbar";
import Footer from "./layouts/Footer";
import Manga from "./manga/Manga";

const Main = () => {
    return (
        <div className={'App d-flex flex-column min-vh-100'}>
            <Loader ref={Loader => {window.Loader = Loader}} />
            <Navbar />
            <div className={'mb-4'}>
                <Routes>
                    <Route path={'/'} element={<Index/>} />
                    <Route path={'/manga/list/:page'} element={<List/>} />
                </Routes>
            </div>
            <Footer />
        </div>
    )
}
export default Main;
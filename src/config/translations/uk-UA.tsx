
export default class Ukrainian {

    private static translations = {
        'Home': 'додому',
    };

    public static get(key: string) {
        return this.translations[key as keyof typeof Ukrainian.translations];
    }

}
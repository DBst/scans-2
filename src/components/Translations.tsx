import React from "react";
import Polish from "../config/translations/pl-PL";
import Config from "../config/main";
import Ukrainian from "../config/translations/uk-UA";
import German from "../config/translations/de-DE";
import Russian from "../config/translations/ru-RU";

export class Translations extends React.Component {

    private language: any;
    private text: string;
    public static languages = {
        en: 'default',
        pl: Polish,
        ua: Ukrainian,
        de: German,
        ru: Russian,
    }

    constructor(props: any) {
        super(props);
        this.text = props.children;

        if(Config.language === null) {
            Config.language = 'en';
        }
        this.setLanguage(Config.language);
    }

    public setLanguage(language: string): boolean {

        let obj = Translations.languages[language as keyof typeof Translations.languages];
        if(obj === null) {
            console.error(`Cannot find language with name: ${language}`)
            return false;
        }
        this.language = obj;
        return true;
    }

    public g = this.get;
    public get(key: string): string {
        if(typeof this.language === 'string' && this.language === 'default') {
            return key;
        }
        return this.language.get(key);
    }

    public detectLanguage(): void {
        let domain = window.location.hostname,
            s = domain.split('.')[0],
            lang = s.length === 2 && Object.keys(Translations.languages).includes(s) ? s : 'en';

        this.setLanguage(lang);
    }

    render() {
        if(!this.language) {
            this.detectLanguage();
        }

        let translatedText = this.get(this.text);
        if(typeof translatedText === 'undefined') {
            return <u><i>#{this.text}#</i></u>;
        }
        return <span>{translatedText}</span>;
    }

}
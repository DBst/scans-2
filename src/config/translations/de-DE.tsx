
export default class German {

    private static translations = {
        'Home': 'Heim',
    };

    public static get(key: string) {
        return this.translations[key as keyof typeof German.translations];
    }

}
import React from 'react';
import './App.css';
import Title from "./components/Title";
import Config from "./config/main";
import Main from "./views/Main";
import './App.css';

Config.language = 'English';

class App extends React.Component {

    constructor(props: any) {
        super(props);
        Title.set('Home');
    }

    render() {
        return <Main />;
    }

}

export default App;


const Footer = () => {
    return (
        <footer className="footer mt-auto">
            <div className="shadow pt-3 pb-3">
                <div className="container">
                    <div className="row">
                        <p className="m-0 col-lg-6">
                            © <a href="http://scans.loc" className="text-dark" style={{textTransform: 'capitalize'}}>
                            scans </a>
                            2021 - 2022 </p>
                        <p className="m-0 col-lg-6 text-end">
                            Coded with <i className="far fa-heart" aria-hidden="true"></i> by
                            <a href="https://www.trojansky.pl" className="text-dark"> Trojansky.pl</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}
export default Footer;
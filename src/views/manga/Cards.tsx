import {Link} from "react-router-dom";
import {Translations as T} from "../../components/Translations";
import {MDBIcon} from "mdb-react-ui-kit";
import React from "react";
import './manga.scss';

const getStars = (rate: number, title: string) => {
    rate = rate / 2;
    let stars = [];
    for(let i = 1; i <= 5; i++) {
        if(rate >= i) {
            stars.push(<MDBIcon key={title + '-rate-' + i} icon='star' fas />);
        } else if(rate + 1/2 === i) {
            stars.push(<MDBIcon key={title + '-rate-' + i} icon='star-half-alt' fas />);
        } else {
            stars.push(<MDBIcon key={title + '-rate-' + i} icon='star' far />);
        }
    }
    return stars;
}

const getMangaUrl = (mangaId: number, title: string) => {
    return `/manga/${mangaId}/${ (title).replaceAll(/[^a-zA-Z]+/g, '_').toLowerCase() }`;
}

export const VerticalCard = (props: any) => {

    return (
        <span className={'p-1 col-lg-3 col-6'}>
            <Link to={getMangaUrl(props.mangaId, props.title)} className={'manga-card'}>
                <div className={'manga-image'} style={{
                    backgroundImage: `url(${props.src})`,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                }}></div>
                <div className={'stars'}>
                    { getStars(props.rate, props.title) }
                </div>
                <p className={'title'}>
                    {props.title}
                </p>
                <p className={'chapters-count bg-primary rounded'}>
                    {props.chapterNumber}
                </p>
            </Link>
        </span>
    )
}

export const HorizontalCard = (props: any) => {

    return (
        <Link to={getMangaUrl(props.mangaId, props.title)} className={'horizontal-card d-flex card mt-2 row'}>
            <div className={"col-auto ps-1 pe-0"}>
                <img src={props.src} alt={props.title} className={"background rounded"} />
            </div>
            <div className={"d-grid col-9"}>
                <h4 className={"m-0"}>
                    { props.title }
                </h4>
                <span className={"stars"}>
                    <h5>
                        { getStars(props.rate, props.title) }
                    </h5>
                </span>
                <span>
                    { props.views } <T>views</T>
                </span>
            </div>
        </Link>
    );

}
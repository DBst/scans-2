
export default class Polish {

    private static translations = {
        'Home': 'Strona Główna',
        'Rating': 'Ocena',
    };

    public static get(key: string) {
        return this.translations[key as keyof typeof Polish.translations];
    }

}
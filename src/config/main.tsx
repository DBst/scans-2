import React from "react";

export default class Config extends React.Component {

    static get language(): string {
        return this._language;
    }

    static set language(value: string) {
        this._language = value;
    }

    private static _language: string = 'English';

}
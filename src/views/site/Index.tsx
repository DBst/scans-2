import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, Parallax } from "swiper";
import {Translations as T} from "../../components/Translations";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import './site.scss';
import mangaImg from '../../assets/img/manga/sololeveling.png';
import React from "react";
import {Link} from "react-router-dom";
import {HorizontalCard, VerticalCard} from "../manga/Cards";
import 'simplebar-react/dist/simplebar.min.css';
import SimpleBarReact from "simplebar-react";

const Index = () => {

    const IndexBanner = () => {
        return (
            <div className={'index-banner'}>
                <Swiper
                    style={{
                        color: '#fff'
                    }}
                    speed={600}
                    parallax={true}
                    pagination={{
                        clickable: true,
                    }}
                    navigation={true}
                    modules={[Parallax, Pagination, Navigation]}
                    className="mySwiper shadow-lg"
                >
                    <div
                        slot="container-start"
                        className="parallax-bg"
                        style={{'backgroundImage': 'url(https://swiperjs.com/demos/images/nature-1.jpg)'}}
                        data-swiper-parallax="-23%"
                    ></div>
                    <SwiperSlide>
                        <h1 className="title" data-swiper-parallax="-300">
                            Slide 1
                        </h1>
                        <div className="text" data-swiper-parallax="-100">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
                                dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
                                laoreet justo vitae porttitor porttitor. Suspendisse in sem justo.
                                Integer laoreet magna nec elit suscipit, ac laoreet nibh euismod.
                                Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
                                ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
                                tincidunt ut libero. Aenean feugiat non eros quis feugiat.
                            </p>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <h1 className="title" data-swiper-parallax="-300">
                            Slide 2
                        </h1>
                        <div className="text" data-swiper-parallax="-100">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
                                dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
                                laoreet justo vitae porttitor porttitor. Suspendisse in sem justo.
                                Integer laoreet magna nec elit suscipit, ac laoreet nibh euismod.
                                Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
                                ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
                                tincidunt ut libero. Aenean feugiat non eros quis feugiat.
                            </p>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <h1 className="title" data-swiper-parallax="-300">
                            Slide 3
                        </h1>
                        <div className="text" data-swiper-parallax="-100">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam
                                dictum mattis velit, sit amet faucibus felis iaculis nec. Nulla
                                laoreet justo vitae porttitor porttitor. Suspendisse in sem justo.
                                Integer laoreet magna nec elit suscipit, ac laoreet nibh euismod.
                                Aliquam hendrerit lorem at elit facilisis rutrum. Ut at
                                ullamcorper velit. Nulla ligula nisi, imperdiet ut lacinia nec,
                                tincidunt ut libero. Aenean feugiat non eros quis feugiat.
                            </p>
                        </div>
                    </SwiperSlide>
                </Swiper>
            </div>
        );
    }

    return (
        <div className={'container'}>
            <div className={'row'}>
                <div className={'col-md-8 text-center mt-4'}>
                    <IndexBanner />
                    <h2 className={'p-1 bg-primary text-white card shadow-lg mt-4'}>
                        <T>Most popular</T>
                    </h2>
                    <div className={'cards row mt-3 me-0 ms-0'}>
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                        <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                    </div>
                    <div className={'w-100 text-center'}>
                        <Link className="btn btn-primary mt-3" to={'/manga/list/1/popular'}>
                            <T>Show more</T>
                        </Link>
                    </div>
                </div>
                <div className={'col-md-4 pb-3 mt-4'}>
                    <h3 className="text-center bg-primary rounded shadow text-white p-1">
                        <T>Last updated</T>
                    </h3>
                    <SimpleBarReact className={'last-updated ps-3 pb-4'} style={{maxHeight: 1400}} forceVisible="y" autoHide={false}>
                        <div className={'cards d-grid'}>
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                            <HorizontalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} views={123} />
                        </div>
                    </SimpleBarReact>
                </div>
            </div>
            <div className={'row mt-3'}>
                <div className={'col-md-12 section'}>
                    <h3 className={'w-100 text-center mt-3 mb-3'}>
                        <T>Last added</T>
                    </h3>
                    <div className={'p-2 pb-1 shadow card'}>
                        <div className={'last-added'}>
                            <Swiper
                                slidesPerView={1}
                                spaceBetween={10}
                                pagination={{
                                    clickable: true,
                                }}
                                modules={[Pagination]}
                                className="mySwiper ps-0 pe-0 pb-2"
                                breakpoints={{
                                    120: {
                                        slidesPerView: 2,
                                    },
                                    640: {
                                        slidesPerView: 2,
                                    },
                                    768: {
                                        slidesPerView: 3,
                                    },
                                    1024: {
                                        slidesPerView: 4,
                                    },
                                    1280: {
                                        slidesPerView: 5,
                                    },
                                    1920: {
                                        slidesPerView: 6,
                                    },
                                }}
                            >
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                                <SwiperSlide>
                                    <VerticalCard mangaId={1} src={mangaImg} rate={7} title={'Solo Leveling'} chapterNumber={123} />
                                </SwiperSlide>
                            </Swiper>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Index;